Suraaj Shrestha
873250163

      This class was actually an interesting pick for me. I was sure on my three
picks for this quarter but not on this one but I heard a second Web programming
class was open. Everybody was interested in this class as I too took the chance
and jumped on this class. I am a little worried as this is a 3000 level course
although I have covered all the prerequisite.
       Since I was a child, I was always interested how these websites worked.
I had no idea of coding as a child and thought all of this was done through
photoshop. Later in my beginner computer class, I was introduced to HTML and
hyper links. This was my first encounter with code which gave me curiosity to
learn more.
        Now the internet and the websites in it has changed. It is fun to use
as well as user friendly at the same time. It has so many contents in a single
page. I was interested in learning to design and make websites and this stage
and hopefully catch up to the changes it will encounter in the coming years.
