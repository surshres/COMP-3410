/*****
Suraaj Shrestha
Project 2
WWW Programming
*******/
var correct = 0;
var answers = [2,2,3,1];
function start()
{
document.getElementById('start').style.display = "none";
document.getElementById('q0').style.display = "block";
}
function finish(a)
{
correct += (document.getElementsByName('a' + a )[answers[a]].checked ? 1:0);
document.getElementById('q'+ a).style.display = "none";
document.getElementById('results').style.display = "block";
document.getElementById('r').innerHTML = correct;
}
function next(a,b)
{
correct += (document.getElementsByName('a' + a)[answers[a]].checked ? 1:0);
document.getElementById('q'+ a).style.display = "none";
document.getElementById('q'+ b).style.display = "block";
}
