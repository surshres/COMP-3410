/********
Suraaj Shrestha
Project 2
WWW Programming
Java Script
********/


/***
Initialization of the variables
****/
var XorO = "";
let TorF = true;
var cells = document.querySelectorAll(".cell");
cells = Array.from(cells);
var table = document.querySelector(".wrapper");
var result = document.querySelector("h1");



/*****
clicks and takes turn to select X's and 0's. Listens to the events and then draws the tic tac toe
also checks to verify each and every cell to see if it is acceptable.
******/
function click()
{
  var count = cells.indexOf(this);
  if(TorF)
  {
    XorO = "X";
    TorF = false;
  }
  else
  {
    XorO = "O";
    TorF = true;
  }
  this.innerHTML = XorO;
  verify(0,1,2);
  verify(3,4,5);
  verify(6,7,8);
  verify(0,3,6);
  verify(1,4,7);
  verify(2,5,8);
  verify(0,4,8);
  verify(2,4,6);
}


/***
for loop to add event listeners for clicks on the cells of the tic tac toe
****/
for (var i = 0; i < cells.length; i++)
{
  cells[i].addEventListener("click",click);
}


/***
for loop to add event listeners for clicks on the cells of the tic tac toe
and print the updated table
****/
function printTable()
{
  table.classList.add("updatedTable");
}


/****
verify each and every step, and then print on the table of cells.
****/
function verify (cell1,cell2,cell3)
{

if(cells[cell1].innerHTML.trim() === cells[cell2].innerHTML.trim()
&& cells[cell2].innerHTML.trim() === cells[cell3].innerHTML.trim()
&& cells[cell1].innerHTML.trim() === cells[cell3].innerHTML.trim()
&& cells[cell1].innerHTML.trim() !== ""
&& cells[cell2].innerHTML.trim() !== ""
&& cells[cell3].innerHTML.trim() !== "")
{
result.innerHTML = XorO + " wins";
printTable();
}
}


/****
a function to reload the game after its over.
*****/
function newGame()
{
location.reload();
}
